EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "core board of Ingenic X2000"
Date "2021-06-02"
Rev "1.0"
Comp "binggee"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Local_Library:X2000 U?
U 1 1 60D1863A
P 7450 2950
F 0 "U?" H 7425 5265 50  0000 C CNN
F 1 "X2000" H 7425 5174 50  0000 C CNN
F 2 "Local_Library:BGA-324_18x18_12.0x12.0mm" H 8950 4350 50  0001 C CNN
F 3 "" H 8950 4350 50  0001 C CNN
	1    7450 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1350 4700 800 
$Comp
L power:GND #PWR0196
U 1 1 62CCB875
P 4000 1100
F 0 "#PWR0196" H 4000 850 50  0001 C CNN
F 1 "GND" H 4005 927 50  0000 C CNN
F 2 "" H 4000 1100 50  0001 C CNN
F 3 "" H 4000 1100 50  0001 C CNN
	1    4000 1100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 1100 4000 1150
$Comp
L power:GND #PWR0197
U 1 1 62CCC49A
P 3200 1200
F 0 "#PWR0197" H 3200 950 50  0001 C CNN
F 1 "GND" H 3205 1027 50  0000 C CNN
F 2 "" H 3200 1200 50  0001 C CNN
F 3 "" H 3200 1200 50  0001 C CNN
	1    3200 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1200 3200 1150
$Comp
L Device:R R?
U 1 1 62CCD5F3
P 4200 1150
F 0 "R?" H 4270 1196 50  0000 L CNN
F 1 "1MR" H 4270 1105 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4130 1150 50  0001 C CNN
F 3 "~" H 4200 1150 50  0001 C CNN
	1    4200 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1300 4200 1450
Wire Wire Line
	4200 1450 4300 1450
Wire Wire Line
	4200 1000 4200 800 
$Comp
L Device:C C?
U 1 1 62CCEDBF
P 3050 950
F 0 "C?" H 2936 996 50  0000 R CNN
F 1 "10pF" H 2936 905 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3088 800 50  0001 C CNN
F 3 "~" H 3050 950 50  0001 C CNN
F 4 "5%" H 3050 950 50  0001 C CNN "Range"
	1    3050 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 62CCFC35
P 3050 1300
F 0 "C?" H 2936 1346 50  0000 R CNN
F 1 "10pF" H 2936 1255 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3088 1150 50  0001 C CNN
F 3 "~" H 3050 1300 50  0001 C CNN
F 4 "5%" H 3050 1300 50  0001 C CNN "Range"
	1    3050 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1150 3050 1100
Wire Wire Line
	3050 800  3500 800 
Connection ~ 3500 800 
Wire Wire Line
	3200 1150 3050 1150
Connection ~ 3050 1150
$Comp
L Device:Crystal Y?
U 1 1 62CD73D4
P 3800 1700
F 0 "Y?" V 3846 1569 50  0000 R CNN
F 1 "32.768K" V 3755 1569 50  0000 R CNN
F 2 "Crystal:Crystal_SMD_MicroCrystal_CC7V-T1A-2Pin_3.2x1.5mm" H 3800 1700 50  0001 C CNN
F 3 "~" H 3800 1700 50  0001 C CNN
	1    3800 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3500 800  3500 850 
Wire Wire Line
	3050 1450 3500 1450
Connection ~ 3500 1450
Connection ~ 3200 1150
$Comp
L Oscillator:XO32 X?
U 1 1 62CC88AA
P 3500 1150
F 0 "X?" V 3454 1494 50  0000 L CNN
F 1 "24M10PPM9PF" V 3545 1494 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_EuroQuartz_XO32-4Pin_3.2x2.5mm" H 4200 800 50  0001 C CNN
F 3 "" H 3400 1150 50  0001 C CNN
	1    3500 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 1550 4700 1650
Wire Wire Line
	4700 1750 4700 1850
Wire Wire Line
	3800 1850 4200 1850
$Comp
L Device:C C?
U 1 1 62CE7A65
P 3350 1700
F 0 "C?" H 3236 1746 50  0000 R CNN
F 1 "22pF" H 3236 1655 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3388 1550 50  0001 C CNN
F 3 "~" H 3350 1700 50  0001 C CNN
F 4 "5%" H 3350 1700 50  0001 C CNN "Range"
	1    3350 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 62CEA717
P 3550 1850
F 0 "C?" V 3802 1850 50  0000 C CNN
F 1 "22pF" V 3711 1850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3588 1700 50  0001 C CNN
F 3 "~" H 3550 1850 50  0001 C CNN
F 4 "5%" H 3550 1850 50  0001 C CNN "Range"
	1    3550 1850
	0    -1   -1   0   
$EndComp
Connection ~ 3800 1550
Connection ~ 3800 1850
$Comp
L power:GND #PWR0198
U 1 1 62CEB534
P 3350 1950
F 0 "#PWR0198" H 3350 1700 50  0001 C CNN
F 1 "GND" H 3355 1777 50  0000 C CNN
F 2 "" H 3350 1950 50  0001 C CNN
F 3 "" H 3350 1950 50  0001 C CNN
	1    3350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1950 3350 1850
Wire Wire Line
	3400 1850 3350 1850
$Comp
L Device:R R?
U 1 1 62CEE7B5
P 4450 1450
F 0 "R?" V 4243 1450 50  0000 C CNN
F 1 "0R" V 4334 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4380 1450 50  0001 C CNN
F 3 "~" H 4450 1450 50  0001 C CNN
	1    4450 1450
	0    1    1    0   
$EndComp
Connection ~ 4200 1450
Wire Wire Line
	3500 800  4200 800 
Connection ~ 4200 800 
Wire Wire Line
	4000 1150 3800 1150
Wire Wire Line
	3700 1850 3800 1850
Wire Wire Line
	3350 1550 3800 1550
Connection ~ 3350 1850
$Comp
L Device:R R?
U 1 1 62CFA37F
P 4350 1850
F 0 "R?" V 4143 1850 50  0000 C CNN
F 1 "0R" V 4234 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4280 1850 50  0001 C CNN
F 3 "~" H 4350 1850 50  0001 C CNN
	1    4350 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 1850 4700 1850
Wire Wire Line
	3800 1550 4700 1550
NoConn ~ 4700 2250
Text Label 4700 2650 2    50   ~ 0
BOOT_SEL0
Text Label 4700 2750 2    50   ~ 0
BOOT_SEL1
Text Label 4700 2850 2    50   ~ 0
BOOT_SEL2
Text Label 4700 1950 2    50   ~ 0
~PRST
Text Label 4700 2050 2    50   ~ 0
PWRON
Text Label 4700 2150 2    50   ~ 0
~WKUP
Text Label 4700 2450 2    50   ~ 0
RTC32K
Text Label 4700 4250 2    50   ~ 0
SFC0_CLK
Text Label 4700 4350 2    50   ~ 0
SFC0_~CE
Text Label 4700 4450 2    50   ~ 0
SFC0_DQ0
Text Label 4700 4550 2    50   ~ 0
SFC0_DQ1
Text Label 4700 4650 2    50   ~ 0
SFC0_DQ2
Text Label 4700 4750 2    50   ~ 0
SFC0_DQ3
Text Label 4700 3150 2    50   ~ 0
MSC0_CLK
Text Label 4700 3250 2    50   ~ 0
MSC0_CMD
Text Label 4700 3350 2    50   ~ 0
MSC0_D0
Text Label 4700 3450 2    50   ~ 0
MSC0_D1
Text Label 4700 3550 2    50   ~ 0
MSC0_D2
Text Label 4700 3650 2    50   ~ 0
MSC0_D3
Text Label 4700 3750 2    50   ~ 0
MSC0_D4
Text Label 4700 3850 2    50   ~ 0
MSC0_D5
Text Label 4700 3950 2    50   ~ 0
MSC0_D6
Text Label 4700 4050 2    50   ~ 0
MSC0_D7
Wire Notes Line
	3950 4100 3100 4100
Wire Notes Line
	3100 4100 3100 3050
Wire Notes Line
	3100 3050 3950 3050
Wire Notes Line
	3950 3050 3950 4100
Text Notes 3650 3600 2    50   ~ 0
EMMC
Text Notes 3750 2750 2    50   ~ 0
BOOT_SEL
Text Notes 3700 4550 2    50   ~ 0
SD CARD
Wire Notes Line
	3100 4150 3900 4150
Wire Notes Line
	3900 4150 3900 4800
Wire Notes Line
	3900 4800 3100 4800
Wire Notes Line
	3100 4800 3100 4150
Text Label 10150 1150 0    50   ~ 0
LCD_D0
Text Label 10150 1250 0    50   ~ 0
LCD_D1
Text Label 10150 1350 0    50   ~ 0
LCD_D2
Text Label 10150 1450 0    50   ~ 0
LCD_D3
Text Label 10150 1550 0    50   ~ 0
LCD_D4
Text Label 10150 1650 0    50   ~ 0
LCD_D5
Text Label 10150 1750 0    50   ~ 0
LCD_D6
Text Label 10150 1850 0    50   ~ 0
LCD_D7
Text Label 10150 2050 0    50   ~ 0
LCD_D8
Text Label 10150 2150 0    50   ~ 0
LCD_D9
Text Label 10150 2250 0    50   ~ 0
LCD_D10
Text Label 10150 2350 0    50   ~ 0
LCD_D11
Text Label 10150 2450 0    50   ~ 0
LCD_D12
Text Label 10150 2650 0    50   ~ 0
LCD_D13
Text Label 10150 2750 0    50   ~ 0
LCD_D14
Text Label 10150 2850 0    50   ~ 0
LCD_D15
Text Label 10150 3050 0    50   ~ 0
LCD_D16
Text Label 10150 3150 0    50   ~ 0
LCD_D17
Text Label 10150 3250 0    50   ~ 0
LCD_D18
Text Label 10150 3350 0    50   ~ 0
LCD_D19
Text Label 10150 3450 0    50   ~ 0
LCD_D20
Text Label 10150 3550 0    50   ~ 0
LCD_D21
Text Label 10150 3650 0    50   ~ 0
LCD_D22
Text Label 10150 3750 0    50   ~ 0
LCD_D23
Text Label 10150 3950 0    50   ~ 0
LCD_PCLK
Text Label 10150 4050 0    50   ~ 0
LCD_VSYNC
Text Label 10150 4150 0    50   ~ 0
LCD_HSYNC
Text Label 10150 4250 0    50   ~ 0
LCD_DE
Text Label 10150 4450 0    50   ~ 0
UART8_RXD
Text Label 10150 4550 0    50   ~ 0
UART8_TXD
Text Label 10150 4650 0    50   ~ 0
UART9_RXD
Text Label 10150 4750 0    50   ~ 0
UART9_TXD
Wire Notes Line
	10700 1100 10700 4250
Wire Notes Line
	10700 4250 11050 4250
Wire Notes Line
	11050 4250 11050 1100
Wire Notes Line
	11050 1100 10700 1100
Text Notes 10800 2200 0    50   ~ 0
LCD
Text Notes 10800 4600 0    50   ~ 0
UART
Wire Notes Line
	10700 4350 11050 4350
Wire Notes Line
	11050 4350 11050 4800
Wire Notes Line
	11050 4800 10700 4800
Wire Notes Line
	10700 4800 10700 4350
Wire Notes Line
	3150 2550 3850 2550
Wire Notes Line
	3850 2550 3850 2900
Wire Notes Line
	3850 2900 3150 2900
Wire Notes Line
	3150 2900 3150 2550
Text Notes 3350 2450 0    50   ~ 0
GPIO
Text Notes 3850 2150 0    50   ~ 0
RESET
Wire Wire Line
	4600 1450 4700 1450
Wire Wire Line
	3500 1450 4200 1450
Wire Wire Line
	4200 800  4700 800 
$Comp
L power:GND #PWR?
U 1 1 61353484
P 1950 6600
F 0 "#PWR?" H 1950 6350 50  0001 C CNN
F 1 "GND" H 1955 6427 50  0000 C CNN
F 2 "" H 1950 6600 50  0001 C CNN
F 3 "" H 1950 6600 50  0001 C CNN
	1    1950 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 6600 1950 6550
Wire Wire Line
	1950 6550 1750 6550
Wire Wire Line
	1750 6550 1750 6500
Connection ~ 1950 6550
Wire Wire Line
	1950 6550 1950 6500
$Comp
L Local_Library:DC1_3.3V #PWR?
U 1 1 613CA903
P 1550 3700
F 0 "#PWR?" H 1550 3700 50  0001 C CNN
F 1 "DC1_3.3V" H 1555 3873 50  0000 C CNN
F 2 "" H 1550 3700 50  0001 C CNN
F 3 "" H 1550 3700 50  0001 C CNN
	1    1550 3700
	1    0    0    -1  
$EndComp
NoConn ~ 1550 6500
NoConn ~ 1300 6500
$Comp
L Local_Library:DC4_1.8V #PWR?
U 1 1 613D31CD
P 1750 3700
F 0 "#PWR?" H 1750 3700 50  0001 C CNN
F 1 "DC4_1.8V" H 1838 3737 50  0000 L CNN
F 2 "" H 1750 3700 50  0001 C CNN
F 3 "" H 1750 3700 50  0001 C CNN
	1    1750 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3700 1550 3800
Wire Wire Line
	1750 3700 1750 3800
Text Label 2400 5700 0    50   ~ 0
MSC0_CLK
Text Label 2400 5550 0    50   ~ 0
MSC0_CMD
Text Label 2400 4650 0    50   ~ 0
MSC0_D0
Text Label 2400 4750 0    50   ~ 0
MSC0_D1
Text Label 2400 4850 0    50   ~ 0
MSC0_D2
Text Label 2400 4950 0    50   ~ 0
MSC0_D3
Text Label 2400 5050 0    50   ~ 0
MSC0_D4
Text Label 2400 5150 0    50   ~ 0
MSC0_D5
Text Label 2400 5250 0    50   ~ 0
MSC0_D6
Text Label 2400 5350 0    50   ~ 0
MSC0_D7
$Comp
L Local_Library:IS21ES64G-JCLI U?
U 1 1 6134C074
P 1650 5100
F 0 "U?" H 2177 5096 50  0000 L CNN
F 1 "IS21ES64G-JCLI" H 2177 5005 50  0000 L CNN
F 2 "Local_Library:BGA-153_14x14_13.0x11.5mm" H 1300 5350 50  0001 C CNN
F 3 "" H 1300 5350 50  0001 C CNN
	1    1650 5100
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 613DA687
P 2500 6050
F 0 "C?" H 2386 6096 50  0000 R CNN
F 1 "1uF" H 2386 6005 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2538 5900 50  0001 C CNN
F 3 "~" H 2500 6050 50  0001 C CNN
F 4 "5%" H 2500 6050 50  0001 C CNN "Range"
	1    2500 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 613DA6AA
P 2500 6300
F 0 "#PWR?" H 2500 6050 50  0001 C CNN
F 1 "GND" H 2505 6127 50  0000 C CNN
F 2 "" H 2500 6300 50  0001 C CNN
F 3 "" H 2500 6300 50  0001 C CNN
	1    2500 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 5900 2500 5850
Wire Wire Line
	2500 5850 2400 5850
Wire Wire Line
	2500 6300 2500 6200
$Comp
L Device:R R?
U 1 1 613EB9FE
P 2500 4100
F 0 "R?" V 2293 4100 50  0000 C CNN
F 1 "10kR" V 2384 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2430 4100 50  0001 C CNN
F 3 "~" H 2500 4100 50  0001 C CNN
	1    2500 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	2400 4350 2500 4350
Wire Wire Line
	2500 4350 2500 4250
$Comp
L Local_Library:DC1_3.3V #PWR?
U 1 1 613EDF39
P 2500 3900
F 0 "#PWR?" H 2500 3900 50  0001 C CNN
F 1 "DC1_3.3V" H 2505 4073 50  0000 C CNN
F 2 "" H 2500 3900 50  0001 C CNN
F 3 "" H 2500 3900 50  0001 C CNN
	1    2500 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3900 2500 3950
$Comp
L Switch:SW_DIP_x03 SW?
U 1 1 613F07D0
P 2300 1100
F 0 "SW?" H 2300 1567 50  0000 C CNN
F 1 "SW_DIP_x03" H 2300 1476 50  0000 C CNN
F 2 "" H 2300 1100 50  0001 C CNN
F 3 "~" H 2300 1100 50  0001 C CNN
	1    2300 1100
	1    0    0    -1  
$EndComp
$Comp
L Local_Library:DC4_1.8V #PWR?
U 1 1 613F1EB4
P 2650 800
F 0 "#PWR?" H 2650 800 50  0001 C CNN
F 1 "DC4_1.8V" H 2655 973 50  0000 C CNN
F 2 "" H 2650 800 50  0001 C CNN
F 3 "" H 2650 800 50  0001 C CNN
	1    2650 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 613F42B6
P 1200 1100
F 0 "R?" V 1315 1100 50  0000 C CNN
F 1 "10kR" V 1406 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 1130 1100 50  0001 C CNN
F 3 "~" H 1200 1100 50  0001 C CNN
	1    1200 1100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 613F619C
P 1450 1000
F 0 "R?" V 1243 1000 50  0000 C CNN
F 1 "10kR" V 1334 1000 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 1380 1000 50  0001 C CNN
F 3 "~" H 1450 1000 50  0001 C CNN
	1    1450 1000
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 613F63EF
P 1200 900
F 0 "R?" V 993 900 50  0000 C CNN
F 1 "10kR" V 1084 900 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 1130 900 50  0001 C CNN
F 3 "~" H 1200 900 50  0001 C CNN
	1    1200 900 
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61404CE6
P 1000 1150
F 0 "#PWR?" H 1000 900 50  0001 C CNN
F 1 "GND" H 1005 977 50  0000 C CNN
F 2 "" H 1000 1150 50  0001 C CNN
F 3 "" H 1000 1150 50  0001 C CNN
	1    1000 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 1150 1000 1100
Wire Wire Line
	1000 900  1050 900 
Connection ~ 1000 1000
Wire Wire Line
	1000 1000 1000 900 
Wire Wire Line
	1050 1100 1000 1100
Connection ~ 1000 1100
Wire Wire Line
	1000 1100 1000 1000
Text Label 1600 900  0    50   ~ 0
BOOT_SEL0
Text Label 1600 1000 0    50   ~ 0
BOOT_SEL1
Text Label 1600 1100 0    50   ~ 0
BOOT_SEL2
Wire Wire Line
	2650 1100 2650 1000
Wire Wire Line
	2600 1000 2650 1000
Connection ~ 2650 1000
Wire Wire Line
	1000 1000 1300 1000
Wire Wire Line
	2650 800  2650 900 
Wire Wire Line
	2000 900  1350 900 
Wire Wire Line
	2000 1000 1600 1000
Wire Wire Line
	2000 1100 1350 1100
Wire Wire Line
	2600 900  2650 900 
Connection ~ 2650 900 
Wire Wire Line
	2650 900  2650 1000
Wire Wire Line
	2650 1100 2600 1100
$Comp
L Device:C C?
U 1 1 6143DD4D
P 1250 2000
F 0 "C?" H 1136 2046 50  0000 R CNN
F 1 "10uF" H 1136 1955 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1288 1850 50  0001 C CNN
F 3 "~" H 1250 2000 50  0001 C CNN
F 4 "5%" H 1250 2000 50  0001 C CNN "Range"
	1    1250 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143DD58
P 1250 2200
F 0 "#PWR?" H 1250 1950 50  0001 C CNN
F 1 "GND" H 1255 2027 50  0000 C CNN
F 2 "" H 1250 2200 50  0001 C CNN
F 3 "" H 1250 2200 50  0001 C CNN
	1    1250 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2200 1250 2150
$Comp
L Device:R R?
U 1 1 6143DD66
P 1600 1800
F 0 "R?" V 1393 1800 50  0000 C CNN
F 1 "10kR" V 1484 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 1530 1800 50  0001 C CNN
F 3 "~" H 1600 1800 50  0001 C CNN
	1    1600 1800
	0    1    1    0   
$EndComp
$Comp
L Device:D D?
U 1 1 61447F2A
P 1600 2050
F 0 "D?" H 1600 2175 50  0000 C CNN
F 1 "WSB5546N" H 1600 2266 50  0000 C CNN
F 2 "" H 1600 2050 50  0001 C CNN
F 3 "~" H 1600 2050 50  0001 C CNN
	1    1600 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 1800 1250 1800
Wire Wire Line
	1250 1800 1250 1850
Wire Wire Line
	1450 2050 1450 1800
Connection ~ 1450 1800
Wire Wire Line
	1800 1700 1800 1800
Wire Wire Line
	1800 2050 1750 2050
Text Label 1450 1800 2    50   ~ 0
~PRST
$Comp
L Device:R R?
U 1 1 614575B2
P 1900 1950
F 0 "R?" H 1830 1904 50  0000 R CNN
F 1 "10kR" H 1830 1995 50  0000 R CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 1830 1950 50  0001 C CNN
F 3 "~" H 1900 1950 50  0001 C CNN
	1    1900 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1750 1800 1800 1800
Connection ~ 1800 1800
Wire Wire Line
	1800 1800 1800 2050
Wire Wire Line
	1800 1800 1900 1800
Wire Wire Line
	1900 2100 1900 2200
Text Label 1900 2200 0    50   ~ 0
~WKUP
$Comp
L Local_Library:RTC_1.8V #PWR?
U 1 1 614641A1
P 1800 1700
F 0 "#PWR?" H 1800 1700 50  0001 C CNN
F 1 "RTC_1.8V" V 1805 1828 50  0000 L CNN
F 2 "" H 1800 1700 50  0001 C CNN
F 3 "" H 1800 1700 50  0001 C CNN
	1    1800 1700
	0    1    1    0   
$EndComp
$EndSCHEMATC
