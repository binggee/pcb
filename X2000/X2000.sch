EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "core board of Ingenic X2000"
Date "2021-06-02"
Rev "1.0"
Comp "binggee"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3800 3250 1100 1100
U 60B8B505
F0 "core" 50
F1 "core.sch" 50
$EndSheet
$Sheet
S 2500 1500 1000 1000
U 60B90AF3
F0 "power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 4500 1500 1100 1000
U 60B9177E
F0 "flash" 50
F1 "flash.sch" 50
$EndSheet
$Sheet
S 5600 3250 1200 1100
U 60E32138
F0 "btob" 50
F1 "btob.sch" 50
$EndSheet
$Sheet
S 2250 3250 1100 950 
U 61311E10
F0 "usb" 50
F1 "usb.sch" 50
$EndSheet
$EndSCHEMATC
