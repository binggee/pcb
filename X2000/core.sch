EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Local_Library:X2000 U?
U 4 1 60D29F84
P 5600 3800
F 0 "U?" H 5850 6165 50  0000 C CNN
F 1 "X2000" H 5850 6074 50  0000 C CNN
F 2 "Local_Library:BGA-324_18x18_12.0x12.0mm" H 7100 5200 50  0001 C CNN
F 3 "" H 7100 5200 50  0001 C CNN
	4    5600 3800
	1    0    0    -1  
$EndComp
Text Label 9200 5600 0    50   ~ 0
UART2_TXD
Text Label 9200 5700 0    50   ~ 0
UART2_RXD
Text Label 9200 5400 0    50   ~ 0
RGMAC0_TX_CLK
Text Label 9200 5300 0    50   ~ 0
RGMAC0_MDIO
Text Label 9200 5200 0    50   ~ 0
RGMAC0_MDC
Text Label 9200 5100 0    50   ~ 0
RGMAC0_RX_DV
Text Label 9200 4900 0    50   ~ 0
RGMAC0_RXD3
Text Label 9200 4800 0    50   ~ 0
RGMAC0_RXD2
Text Label 9200 4700 0    50   ~ 0
RGMAC0_RXD1
Text Label 9200 4600 0    50   ~ 0
RGMAC0_RXD0
Text Label 9200 4500 0    50   ~ 0
RGMAC0_TXD3
Text Label 9200 4400 0    50   ~ 0
RGMAC0_TXD2
Text Label 9200 4200 0    50   ~ 0
RGMAC0_TXD0
Text Label 9200 4100 0    50   ~ 0
RGMAC0_PHY_CLK
Text Label 9200 3900 0    50   ~ 0
PWM0
Text Label 9200 4300 0    50   ~ 0
RGMAC0_TXD1
Text Label 9200 5000 0    50   ~ 0
RGMAC0_TX_EN
Text Label 9200 3100 0    50   ~ 0
TRST
Text Label 9200 3300 0    50   ~ 0
TMS
Text Label 9200 3400 0    50   ~ 0
TCK
Text Label 9200 3500 0    50   ~ 0
TDI
Text Label 9200 3600 0    50   ~ 0
TDO
Text Label 9200 3700 0    50   ~ 0
PD00
Text Label 9200 3200 0    50   ~ 0
PD05
Text Notes 10200 5650 0    50   ~ 0
UART
Text Notes 10200 4700 0    50   ~ 0
GMAC
Text Notes 10200 3900 0    50   ~ 0
PWM
Text Notes 10200 3400 0    50   ~ 0
JTAG
Text Notes 10200 2650 0    50   ~ 0
WIFI
Text Label 9200 2900 0    50   ~ 0
SDIO_D3
Text Label 9200 2800 0    50   ~ 0
SDIO_D2
Text Label 9200 2700 0    50   ~ 0
SDIO_D1
Text Label 9200 2600 0    50   ~ 0
SDIO_D0
Text Label 9200 2500 0    50   ~ 0
SDIO_CMD
Text Label 9200 2400 0    50   ~ 0
SDIO_CLK
Wire Notes Line
	10000 5500 10500 5500
Wire Notes Line
	10500 5500 10500 5750
Wire Notes Line
	10500 5750 10000 5750
Wire Notes Line
	10000 5750 10000 5500
Wire Notes Line
	10000 5450 10500 5450
Wire Notes Line
	10500 5450 10500 4050
Wire Notes Line
	10500 4050 10000 4050
Wire Notes Line
	10000 4050 10000 5450
Wire Notes Line
	10000 3950 10500 3950
Wire Notes Line
	10500 3950 10500 3800
Wire Notes Line
	10500 3800 10000 3800
Wire Notes Line
	10000 3800 10000 3950
Wire Notes Line
	10500 3750 10500 3050
Wire Notes Line
	10000 3050 10000 3750
Wire Notes Line
	10000 3750 10500 3750
Wire Notes Line
	10000 3050 10500 3050
Wire Notes Line
	10000 2950 10500 2950
Wire Notes Line
	10500 2950 10500 2350
Wire Notes Line
	10500 2350 10000 2350
Wire Notes Line
	10000 2350 10000 2950
Text Label 9200 1800 0    50   ~ 0
DMIC_CLK
Text Label 9200 1900 0    50   ~ 0
DMIC_IN0
Text Label 9200 2000 0    50   ~ 0
DMIC_IN1
Text Notes 10200 1900 0    50   ~ 0
DMIC
Text Notes 10200 2200 0    50   ~ 0
I2C
Text Label 9200 2100 0    50   ~ 0
I2C1_SCK
Text Label 9200 2200 0    50   ~ 0
I2C1_SDA
Wire Notes Line
	10000 2250 10500 2250
Wire Notes Line
	10500 2250 10500 2050
Wire Notes Line
	10500 2050 10000 2050
Wire Notes Line
	10500 2000 10500 1750
Wire Notes Line
	10500 1750 10000 1750
Wire Notes Line
	10500 2000 10000 2000
Wire Notes Line
	10000 2000 10000 1750
Wire Notes Line
	10000 2050 10000 2250
Text Label 2500 1800 2    50   ~ 0
CIM_VIC_D0
Text Label 2500 1900 2    50   ~ 0
CIM_VIC_D1
Text Label 2500 2000 2    50   ~ 0
CIM_VIC_D2
Text Label 2500 2100 2    50   ~ 0
CIM_VIC_D3
Text Label 2500 2200 2    50   ~ 0
CIM_VIC_D4
Text Label 2500 2300 2    50   ~ 0
CIM_VIC_D5
Text Label 2500 2400 2    50   ~ 0
CIM_VIC_D6
Text Label 2500 2500 2    50   ~ 0
CIM_VIC_D7
Text Label 2500 2600 2    50   ~ 0
CIM_VIC_D8
Text Label 2500 2700 2    50   ~ 0
CIM_VIC_D9
Text Label 2500 2800 2    50   ~ 0
CIM_VIC_D10
Text Label 2500 2900 2    50   ~ 0
CIM_VIC_D11
Text Label 2500 3100 2    50   ~ 0
CIM_VIC_HSYNC
Text Label 2500 3200 2    50   ~ 0
CIM_VIC_VSYNC
Text Label 2500 3300 2    50   ~ 0
CIM_VIC_PCLK
Text Label 2500 3400 2    50   ~ 0
CIM_EXPOSURE
Text Label 2500 3600 2    50   ~ 0
I2C3_SCK
Text Label 2500 3700 2    50   ~ 0
I2C3_SDA
Text Label 2500 4100 2    50   ~ 0
RGMAC0_RX_CLK
Text Label 2500 3900 2    50   ~ 0
CIM_VIC_MCLK
Text Label 2500 4300 2    50   ~ 0
DRV_VBUS
Text Notes 1500 3650 0    50   ~ 0
I2C
Wire Notes Line
	1300 3700 1800 3700
Wire Notes Line
	1800 3700 1800 3500
Wire Notes Line
	1800 3500 1300 3500
Wire Notes Line
	1300 1750 1850 1750
Wire Notes Line
	1850 1750 1850 3950
Wire Notes Line
	1850 3950 1300 3950
Wire Notes Line
	1300 1750 1300 3950
Text Notes 1500 2750 0    50   ~ 0
CIM
Text Notes 1500 4550 0    50   ~ 0
UART
Wire Notes Line
	1300 4600 1800 4600
Wire Notes Line
	1800 4600 1800 4400
Wire Notes Line
	1800 4400 1300 4400
Text Notes 1500 4800 0    50   ~ 0
I2C
Wire Notes Line
	1300 4850 1800 4850
Wire Notes Line
	1800 4850 1800 4650
Wire Notes Line
	1800 4650 1300 4650
Text Label 2500 4700 2    50   ~ 0
UART3_RTS
Text Label 2500 4800 2    50   ~ 0
UART3_CTS
Text Label 2500 4500 2    50   ~ 0
UART3_TXD
Text Label 2500 4600 2    50   ~ 0
UART3_RXD
Wire Notes Line
	1300 4400 1300 4600
Wire Notes Line
	1300 4650 1300 4850
Text Label 2500 5200 2    50   ~ 0
PWM4
Text Label 2500 5300 2    50   ~ 0
PWM5
Text Label 2500 5000 2    50   ~ 0
PWM2
Text Label 2500 5100 2    50   ~ 0
PWM3
Text Label 2500 5400 2    50   ~ 0
PWM6
Text Label 2500 5500 2    50   ~ 0
PWM7
Text Notes 1500 5300 0    50   ~ 0
PWM
Wire Notes Line
	1300 4950 1800 4950
Wire Notes Line
	1800 4950 1800 5550
Wire Notes Line
	1800 5550 1300 5550
Wire Notes Line
	1300 5550 1300 4950
$EndSCHEMATC
